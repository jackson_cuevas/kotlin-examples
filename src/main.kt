import kotlin.math.sqrt
import kotlin.random.Random

fun main(args: Array<String>) {
/*
val temperature = 37.5
val count : Int = 5
var name = "Sam"

name = "Fred"
*/

/*
fun calculateTemperature(celsius : Double) : Double {
    return 9/5 * celsius + 32
}

println("Temp = ${calculateTemperature(20.0)}")
println("Temp = ${calculateTemperature(50.0)}")
*/
/*
val intValue = "32".toInt()
println("inValue = $intValue")

val intString = 32.toString()
println("intString = $intString")
*/

/*
val fahrenheit = 32
when (fahrenheit) {
    in 0..30 -> println("really cold")
    in 31..40 -> println("Getting colder")
    in 41..50 -> println("Kind of cold")
    in 51..60 -> println("Nippy")
    in 71..80 -> println("just right")
}
*/

/*var nullableName : String? = null
var length = nullableName?.length ?: -1
println(length)
nullableName = "Sam"
length = nullableName?.length ?: -1
println(length)
*/

//Pair and Triple
/*
    val coordinates = Pair(2,3)
    val (x, y) = coordinates
    println("x = $x y = $y")

    println("x = ${coordinates.first} y = ${coordinates.second}")

    val coordinatesDouble = Pair(2.1, 3.5)
    println("x = ${coordinatesDouble.first} y = ${coordinatesDouble.second}")

    val coordinatesMixed = Pair(2.1, 3)
    println("x = ${coordinatesMixed.first} y = ${coordinatesMixed.second}")

    val x1 = coordinates.first
    val y1 = coordinates.second
    println("x1 = ${x1} y1 = ${y1}")

    val coordinates3D = Triple(2,3,1)
    val (x3,y3,z3) = coordinates3D
    println("x3 = ${x3} y3 = ${y3} z3 = ${z3}")

    val (x4, y4, _) = coordinates3D
    println("x4 = $x4 y4 = $y4")


*/

/* Pairs and Triples

Declare a constant Pair that contains two Int values. Use this to represent a date (month, day).
*/

/*val date = Pair(4,2019)
val (month,day) = date
println("$month/$day")*/

/*
Now create a Triple using the month, day and year */

/*
val date = Triple(4,5,2019)
val (month,day,year) = date
println("$month/$day/$year")*/

//Boolean
/*
val yes1: Boolean = true
val no1: Boolean = false

val yes2 = true
val no2 = false

println("yes1 = yes2 ${yes1 == yes2} no1 = no2 ${no1 == no2}")

val doesOneEqualTwo= (1 == 2)
println("Does 1 equal 2 = ${doesOneEqualTwo}")

val doesOneNotEqualTwo = ( 1 != 2)
println("Does 1 not equal 2 = ${doesOneNotEqualTwo}")

val longName = "Samantha".length > 5
println("Longname = ${longName}")

val and = true && true
println("and = $and")

val or = true || true
println("or = $or")

val guess = "dog"
val dogEqualCat = guess == "cat"
println("dogEqualCat ${dogEqualCat}")

val a = 5
val b = 10

val min : Int
if(a < 5) {
    min = a
}else {
    min = b
}

println("min = $min") */

/*
BOOLEANS
Create a constant called `myAge` and set it to your age.
Then, create a constant called `isTeenager` that uses Boolean logic
to determine if the age denotes someone in the age range of 13 to 19.
*/
//    val myAge = 30
//    val isTeenager : Boolean
//
//    if (myAge >= 13 && myAge <= 19)
//    {
//        isTeenager = true
//    }
//    else {
//        isTeenager = false
//    }
//
//    println("Is a Teenager? = ${isTeenager}")
/*
 Create another constant called `theirAge` and set it to the age of an person which is 30.
 Then, create a constant called `bothTeenagers` that uses
 Boolean logic to determine if both you and him are teenagers.
 */
//    val theirAge = 19
//    val bothTeenagers : Boolean
//    if ((myAge >= 13 && myAge <= 19) && (theirAge >= 13 && theirAge <= 19) )
//    {
//        bothTeenagers = true
//    }
//    else{
//        bothTeenagers = false
//    }
//
//    print("Both are Teenager $bothTeenagers")

/*
 Create a constant called student and set it to your name as a string.
 Create a constant called author and set it to Kevin Moore.
 Create a constant called `authorIsStudent` that uses string equality
 to determine if student and author are equal.
 */
//    val student = "Jackson Cuevas"
//    val author = "Kevin Moore"
//    val autorIsStudent = student == author
//    println("Student and author are equal? $autorIsStudent")


/*
 Create a constant called `studentBeforeAuthor` which uses string
 comparison to determine if student comes before author.
 */

//    val studentBeforeAuthor = student < author
//    println("studentBeforeAuthor = $studentBeforeAuthor")
/*
 IF STATEMENTS AND BOOLEANS
 Use the constant called `myAge` and initialize it with your age.
 Write an if statement to print out Teenager if your age is between 13 and 19,
 and Not a teenager if your age is not between 13 and 19.
 */
//    when(myAge)
//    {
//        in 13..19 -> println("Its a Teenager!")
//
//        else -> println("It's not a Teenager!")
//    }
/*
 Create a constant called `answer` and an if expression to set it
 equal to the result you print out for the same cases in the above exercise.
 Then print out answer.
 */

//    val answer = if (myAge >= 13 && myAge <= 19) "Teenager" else "Not a teenager"
//    println(answer)

//SCOPE
//
//    var hoursWorked = 45
//    var price = 0.0
//    if (hoursWorked > 40) {
//        val hoursOver40 = hoursWorked - 40
//        price += hoursOver40 * 50
//        hoursWorked -= hoursOver40
//    }
//
//    price += hoursWorked * 25
//    println(price)

//FLOW CONTROL

//    while (/*condition*/){
//        //loop code
//    }
/*
    var i =1
    while (i < 10) {
        print(i)
        i += 1
    }
    println("-----")

//    do {
//        //loop code
//    }while (/* condition */)
//
    i = 1
    do {
        print(i)
        i += 1
    }while (i < 10)
    println("-----")

    i = 1
    do {
        print(i)
        if (i == 5){
            break
        }
        i += 1
    }while (i < 10)
    println("After do" */

/*
### WHILE LOOPS
Create a variable named `counter` and set it equal to 0. Create a `while` loop with
the condition `counter < 10` which prints out `counter` is `X`
(where `X` is replaced with counter value) and then increments `counter` by 1.
*/
//    var counter = 0
//    while (counter < 10){
//        println("counter is $counter")
//        counter += 1
//    }

/*
 Create a variable named count and set it equal to 0.
 Create another variable named roll and set it equal to 0.
 Create another variable named random and set it equal to Random().
 Create a do-while loop. Inside the loop, set roll equal to random.nextInt(6)
 which means to pick a random number between 0 and 5. Then increment counter by 1.
 Finally, print 'After X rolls, roll is Y' where X is the value of
 count and Y is the value of roll. Set the loop condition such that the
 loop finishes when the first 0 is rolled.
 */


//    var count = 0
////    var roll = 0
////    val random = Random(6)
////
////    do{
////        roll = random.nextInt(6)
////        count += 1
////        println("After $count rolls, roll is $roll ")
////
////    }while ( roll != 0)

//FOR LOOP
/*
    val range = 0..5

//    for (/* condition in range */) {
//        //Loop code
//    }

    val count = 10
    var sum = 0
    for (i in 1..count) {
        sum += i
    }
    println("sum = $sum")

    for (i in 0..count) {
        println("Hodor!")
    }

    for (i in 1..count){
        if (i %2 == 1) {
            println("$i is an odd")
        }
    }

    for (i in 1..count) {
        println("Hello.")
        if( i == 3) {
            continue
        }
        println("Goodbye")
    }

    for (row in 1..3){
        for (column in 1..3) {
            print("$column")
        }
        println()
    }

    outer@ for (row in 1..3) {
        for (column in 1..3) {
            if (row == 2 && column ==2) {
                println()
                continue@outer
            }
            print("$column")
        }
        println()
    }
    */
/**
 * Create a constant called range and set it equal to a range
 * starting at 1 and ending with 10 inclusive.
 * Write a for loop which iterates over this range and print the
 * square of each number
 */

//        val range = 1..10
//        for (i in range){
//            println(i * i)
//        }

/**
 * Write a for loop to iterate over the same range as in the exercise above
 * and print the square root of each number using the sqrt function.
 * You'll need to convert your loop constant
 */
//     for (i in range){
//         val squareRoot = sqrt(i.toDouble())
//         println(squareRoot)
//     }

/**
 * Create a variable named sum. Write a for loop with a constant named row
 * starting at 0 and going to 8. Inside of the loop check the row variable
 * and if it is odd, add another for loop that uses a column variable to go
 * from 0 to 8. In that loop add to the sum variable the muliptlication of
 * row * column.
 * Then print out sum
 */
//    var sum = 0
//    for (row in 0..8){
//        if (row % 2 == 1) {
//            for (column in 0..8)
//            {
//                sum += row * column
//            }
//        }
//
//    }
//    print("$sum")


//WHEN EXPRESION
/* val number = 10
 when (number) {
     0 -> println("Zero")
     10 -> println("It's ten!")
     else -> println("Non-zero")
 }

 val string = "Dog"
 when (string) {
     "Cat", "Dog" -> println("Animal is a house pet")
     else -> println("Animal is not a house pet")
 }
 when (number) {
     in 1..9 -> println("Between 1 and 9 ")
     in 10..20 -> println("Between 10 and 20")
     else -> println("Some other number")
 }

 when {
     number % 2 == 0 -> println("Even")
     else -> println("Odd")
 }

 val testValue = 10
 val result = when {
     testValue < 10 -> "Less than 10"
     testValue > 10 -> "Greater than 10"
     else -> "is equal to 10"
 }

 print(result)
*/

/*
### when STATEMENTS

Write a when statement that takes an age as an integer and prints out the life stage related to
that age. You can make up the life stages, or use my categorization as follows: 0-2 years,
Infant; 3-12 years, Child; 13-19 years, Teenager; 20-39, Adult;40-60, Middle aged; 61+, Elderly.
*/
/*

var age = 59
when(age)
{
    in 0..2 -> print("Infant")
    in 3..12 -> print("Child")
    in 13..19 -> print("Teenager")
    in 20..39 -> print("Adult")
    in 40..60 -> print("Middle aged")
    in 61..100 -> print("Ederly")
    else
        -> print("Out of range")
}
*/

/*
 Write a when statement that takes a pair containing a string and an integer.
 The string is a name, and the integer is an age. Use the same cases that you used
 in the previous exercise and println out the name followed by the life stage.
 For example, for the author of these challenges, it would println out "Kevin is middle aged.".
 */

//    var data = Pair("Jackson",10)
//    when(data.second) {
//        in 0..2 -> print("${data.first} is Infant")
//        in 3..12 -> print("${data.first} is Child")
//        in 13..19 -> print("${data.first} is Teenager")
//        in 20..39 -> print("${data.first} is Adult")
//        in 40..60 -> print("${data.first} is Middle aged")
//        in 61..100 -> print("${data.first} is Ederly")
//
//        else -> print("Not input")
//    }

/*
 * Write a when expression using the same when(myAge)  as above but
 * set the result to personsAge and then print it.
 */
/*
var myAge = 18
var personsAge = when(myAge) {
    in 0..2 -> "Infant"
    in 3..12 -> "Child"
    in 13..19 -> "Teenager"
    in 20..39 -> "Adult"
    in 40..60 -> "Middle aged"
    in 61..100 -> "Ederly"
    else -> "Out of range"
}
println(personsAge)

}